Why Kotlin?
==
* **100% interoperable with Java** - Kotlin and Java can co-exist in one project. 
You can continue to use existing libraries in Java.
* **Concise** - Drastically reduce the amount of boilerplate code you need to write.
* **Safe** - Avoid entire classes of errors such as null pointer exceptions.
* **It's functional** - Kotlin uses many concepts from functional programming, such as lambda expressions.
* **It's FREE** - it's open source in github