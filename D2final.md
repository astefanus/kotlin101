More readings
==
* Kotlin Docs:
https://kotlinlang.org/docs
* Kotlin Koans: https://kotlinlang.org/docs/tutorials/koans.html
* Getting started with Android and Kotlin: https://kotlinlang.org/docs/tutorials/kotlin-android.html
* Swift ~ Kotlin: http://nilhcem.com/swift-is-like-kotlin/