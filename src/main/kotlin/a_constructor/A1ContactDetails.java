package a_constructor;

public class A1ContactDetails {
    private String firstName;
    private String lastName;
    private String address;

    public A1ContactDetails(String firstName,
                            String lastName,
                            String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public A1ContactDetails(String firstName,
                            String address) {
        this(firstName, "N/A", address);
    }

    public A1ContactDetails(String firstName) {
        this(firstName, "N/A");
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("A1ContactDetails{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
        A1ContactDetails A1ContactDetails = new A1ContactDetails("Arvin");

        System.out.println(A1ContactDetails);
    }
}
