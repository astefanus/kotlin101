

// CONSTRUCTOR




package a_constructor

class A2ContactDetails(val firstName: String,
                       val lastName: String = "N/A",
                       val address: String = "N/A") {

    override fun toString(): String {
        return "A2ContactDetails(" +
                "firstName=" +
                "'$firstName', " +
                "lastName='$lastName', " +
                "address='$address')"
    }
}

fun main(args: Array<String>) {
//    println(A2ContactDetails("Arvin"))
//    println(A2ContactDetails(lastName = "Stefanus", firstName = "Arvin"))
//    println(A2ContactDetails())

//    TODO: use data

//    HOW TO DO SETTER
//    val kContactDetails = A2ContactDetails("Arvin")
//    kContactDetails.address = "Wolli Creek"
//    println(kContactDetails)
}
