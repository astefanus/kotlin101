

// LAMBDA





package c_function


fun main(args: Array<String>) {
    val allNumbers = listOf(
            "0434423422",
            "0423412332",
            "01231",
            "2131231233",
            "3213123131",
            "0313213433",
            "03132134311213")

    // Lambda - 'it' expression
    val phoneNumbers = allNumbers
            .filter {
                it.startsWith("0") && it.length >= 10
            }
            .map { rawPhoneNumber ->
                rawPhoneNumber.formatAsPhoneNumber()
            }

    println(phoneNumbers)
}