

// EXTENSION
// Java Util




package c_function

//String template
fun String.formatAsPhoneNumber() = "+61 ${this.substring(1, 4)} ${this.substring(4, 7)}  ${this.substring(7)}"

fun main(args: Array<String>) {
    println("0434513194".formatAsPhoneNumber())
}