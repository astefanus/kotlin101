

// HIGHER ORDER FUNCTION
// a function that takes functions as parameters,
// or returns a function.





package c_function


fun calculator(a: Int, b: Int, operator: (Int, Int) -> Int)
        = operator(a, b)







fun main(args: Array<String>) {
//    fun add(a: Int, b: Int) = a + b
//    println("add: ${calculator(1, 5, ::add)}")





//    val multiply = { a: Int, b: Int -> a * b }
//    println("multiply: ${calculator(1, 5, multiply)}")







//    println("subtract: ${calculator(1, 5, { x, y -> x - y })}")
}